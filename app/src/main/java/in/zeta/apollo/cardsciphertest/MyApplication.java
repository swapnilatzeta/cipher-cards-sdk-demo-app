package in.zeta.apollo.cardsciphertest;

import android.app.Application;
import android.util.Base64;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

import in.android.zeta.cipher.CipherBuilder;
import in.android.zeta.cipher.CipherService;
import in.zeta.android.commons.exceptions.ApolloException;
import in.zeta.android.commons.kvstore.BasicKVStore;
import in.zeta.apollo.cards.CardsService;
import in.zeta.apollo.cards.CardsServiceBuilder;
import in.zeta.apollo.crypto.EncryptedStoreService;
//import io.reactivex.subjects.PublishSubject;
import timber.log.Timber;

public class MyApplication extends Application {

    private static CipherService cipherService;
    private static CardsService cardsService;

    private BasicKVStore store;

    private static final String STORE_NAME = "tenantKVStore";

    private static final String MANROPE3_REGULAR_FONT_PATH = "fonts/Manrope3-Regular.otf";
    private static final String MANROPE3_MEDIUM_FONT_PATH = "fonts/Manrope3-Medium.otf";
    private static final String MANROPE3_SEMI_BOLD_FONT_PATH = "fonts/Manrope3-Semibold.otf";
    private static final String MANROPE3_BOLD_FONT_PATH = "fonts/Manrope3-Bold.otf";

    private static final Map<String, String> fontMap = new HashMap<>();

    //public PublishSubject<Throwable> exceptionPublisher = PublishSubject.create();

    static {
        fontMap.put("Manrope3-Regular".toLowerCase(), MANROPE3_REGULAR_FONT_PATH);
        fontMap.put("Manrope3-Medium".toLowerCase(), MANROPE3_MEDIUM_FONT_PATH);
        fontMap.put("Manrope3-Semibold".toLowerCase(), MANROPE3_SEMI_BOLD_FONT_PATH);
        fontMap.put("Manrope3-Bold".toLowerCase(), MANROPE3_BOLD_FONT_PATH);
    }

    //public PublishSubject<Throwable> exceptionPublisher = PublishSubject.create();

    public static MyApplication APPLICATION_INSTANCE;

    /*private EncryptedStoreService.AppSecuredStoreAccess securedStoreAccess = new EncryptedStoreService.AppSecuredStoreAccess() {

        @Override
        public void setDataInSecureStore(@NonNull String s, byte[] bytes, @NonNull SetaDataInSecureStoreListener listener) {
            store.putString(s, Base64.encodeToString(bytes, Base64.NO_WRAP));
            listener.done(true, null);
        }

        @Override
        public void getDataFromSecureStore(@NonNull String s, @NonNull GetDataInSecureStore listener) {
            listener.done(Base64.decode(store.getString(s), Base64.NO_WRAP), null);
        }

        @Override
        public boolean contains(@NonNull String s) {
            return store.contains(s);
        }

    };*/

    /*private EncryptedStoreService.SupportedSecurityStores methodsSupport = new EncryptedStoreService.SupportedSecurityStores() {
        @Override
        public boolean supportDeviceLock() {
            return false;
        }

        @Override
        public boolean useOwnSecureStore() {
            return false;
        }
    };*/

    private CipherService.SuperPinSetupExceptionListener exceptionListener = new CipherService.SuperPinSetupExceptionListener() {
        @Override
        public void onException(@NotNull Throwable throwable) {
            //exceptionPublisher.onNext(throwable);
            if(throwable instanceof ApolloException) {
                Timber.e("@superpinexcep: The exception code is: " + ((ApolloException) throwable).getCode());
                Timber.e("@superpinexcep: The exception trace id: " + ((ApolloException) throwable).getTraceId());
                Timber.e("@superpinexcep: The exception msg is: " + ((ApolloException) throwable).getMessage());
            }
            Timber.e("Exception thrown from the cipher sdk", throwable);
        }
    };

    /*private CipherService.SetPushTokenExceptionListener pushTokenExceptionListener = throwable -> {
        if(throwable instanceof ApolloException) {
            Timber.e("@pushexcep: The exception code is: " + ((ApolloException) throwable).getCode());
            Timber.e("@pushexcep: The exception trace id: " + ((ApolloException) throwable).getTraceId());
            Timber.e("@pushexcep: The exception msg is: " + ((ApolloException) throwable).getMessage());
        }
        Timber.e("Exception thrown from the cipher sdk", throwable);
    };*/


    @Override
    public void onCreate() {
        super.onCreate();

        APPLICATION_INSTANCE = this;

        store = new BasicKVStore(this, STORE_NAME);

        Timber.plant(new Timber.DebugTree());

        setupAcs();

        setupUpi();
    }

    private void setupAcs() {
        cipherService = new CipherBuilder(this)
                .setSuperPinExceptionListener(exceptionListener)
                //.setTenantSecuredStoreAccess(securedStoreAccess)
                .setUseDeviceLock(true)
                .setNotificationIcon(R.drawable.ic_stat_adb)
                .setAppDisplayName(R.string.display_name)
                .setFontMap(fontMap)
                .build();

        // Trying to register in case the use is not registered yet ...
        //acsInternalService.register("+918948763891", "phoneNumber");
    }

    public static CipherService getCipherService() {
        return cipherService;
    }


    private void setupUpi() {
        new CardsServiceBuilder(this)
                .setLoggingEnabled(true)
                .setAppDisplayName(R.string.display_name)
                .setUseDeviceLock(true)
                .setFontMap(fontMap)
                .build();
        cardsService = CardsService.getInstance();
    }

    public static CardsService getCardsService() {
        return cardsService;
    }


}
