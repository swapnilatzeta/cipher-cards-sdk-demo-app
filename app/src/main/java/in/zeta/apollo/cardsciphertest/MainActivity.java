package in.zeta.apollo.cardsciphertest;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import in.android.zeta.cipher.CipherService;
//import in.android.zeta.cipher.pushmessage.AcsRequestOTP;
//import in.android.zeta.cipher.pushmessage.AcsRequestOtpAmount;
import in.android.zeta.cipher.pushmessage.AcsRequestOtpAmount;
import in.android.zeta.cipher.pushmessage.CardInfo;
import in.android.zeta.cipher.pushmessage.PayloadVersion1;
import in.android.zeta.cipher.pushmessage.PayloadVersions;
import in.zeta.android.commons.exceptions.ApolloException;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private Button button;
    private TextView appState;
    private EditText phoneNumber;
    private Button swipeToPay;
    private Button logout;
    private Button refreshSwipes;
    private Button cardsSdk;

    private TextView superPinExp;

    private CipherService cipherService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cipherService = MyApplication.getCipherService();

        cipherService.setParentActivityForAuth(this);

        cipherService.setCipherStateUpdateListener(newState -> {
            Log.d(TAG, "In new state: " + newState);
            new Handler(Looper.getMainLooper()).post(() -> {
                Toast.makeText(getApplicationContext(), "The new state is: " + newState, Toast.LENGTH_SHORT).show();
                setViewBasedOnState();
            });
        });

        /*acsService.authenticate("eyJhbGciOiJFUzI1NiJ9.eyJ1c2VySUQiOiI4NDQ3MzkiLCJpYXQiOjE1Nzc5NDgxNDcsImV4cCI6MTU3Nzk4NDE0N30.BFbsSE-Ul1n-eD_jk16LzzdFdOEPwAfTpjwn25tiQ3-5f7SJjPSxxh2s8g4m34FDqVg2uPIIihZk3gYlS_X3_A");*/

        setupView();

        setViewBasedOnState();
    }

    private void setupView() {
        button = findViewById(R.id.btGo);
        appState = findViewById(R.id.tvAppState);
        phoneNumber = findViewById(R.id.etPhoneNumber);
        swipeToPay = findViewById(R.id.btSwipeToPay);
        logout = findViewById(R.id.btLogout);
        superPinExp = findViewById(R.id.tvSuperPinExp);
        refreshSwipes = findViewById(R.id.btRefreshSwipes);
        cardsSdk = findViewById(R.id.btCardsSdk);

        //swipeToPay.setVisibility(View.GONE);

        cardsSdk.setOnClickListener(view -> {
            final Intent i = new Intent(this, CardDetailsActivity.class);
            startActivity(i);
        });

        refreshSwipes.setOnClickListener(view -> {
            cipherService.refreshPendingSwipeRequests((success, exp) -> {
                Timber.d("Success in  refreshing the swipe requests: " + success);
                Timber.d(exp, "Exception in refreshing the swipe requests");
            });
        });

        swipeToPay.setOnClickListener(view -> {


            final AcsRequestOtpAmount amount = new AcsRequestOtpAmount(123, "INR", 2);

            final CardInfo cardInfo = new CardInfo(
                    "1234569993xxxxx42",
                    "rupay",
                    "",
                    "",
                    "");

            final PayloadVersion1 payloadVersion1 = new PayloadVersion1("acsProtocaol", "challengeInput", 1, 2, "authenContextID", "Flipkart", amount, null, "credentialId", cardInfo);

            final PayloadVersions payload = new PayloadVersions(payloadVersion1);

            cipherService.testSwipeToPay(this, payload);

        });

        logout.setOnClickListener(view -> {
            printMsg("Logout success. Start over ...");
            cipherService.logout();
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takeActionBasedOnState();
            }
        });
    }

    private void setViewBasedOnState() {

        Log.d(TAG, "cipherService.isSecureStoreSetupRequired(): " + cipherService.isSecureStoreSetupRequired());
        Log.d(TAG, "cipherService.isSuperPinSetupRequired()): " + cipherService.isSuperPinSetupRequired());
        Log.d(TAG, "cipherService.isSwipeToPaySetupDone(): " + cipherService.isSuperPinSetupDone());


        button.setVisibility(View.INVISIBLE);
        button.setActivated(true);
        logout.setVisibility(View.VISIBLE);

        if(cipherService.isSecureStoreSetupRequired()) {
            appState.setText("Setup secured store");
            button.setVisibility(View.VISIBLE);
            button.setText("Setup secure store");
        } else if(cipherService.isSuperPinSetupDone()){
            button.setVisibility(View.VISIBLE);
            appState.setText("Swipe to Pay Setup done");
            button.setText("Get Super PIN");
        } else if(cipherService.isSuperPinSetupRequired()){
            button.setVisibility(View.VISIBLE);
            appState.setText("Setup super pin");
            button.setText("Setup super pin");
        }
    }

    private void takeActionBasedOnState() {

        final String authToken = phoneNumber.getText().toString();
        if(authToken.trim().length() == 0) {
            Toast.makeText(this, "Please enter a auth token", Toast.LENGTH_SHORT).show();
            return;
        }

        cipherService.registerSDK(authToken);

        button.setEnabled(false);

        if(cipherService.isSecureStoreSetupRequired()) {
            cipherService.setupSecuredStore(this, (b, exp) -> {
                new Handler(Looper.getMainLooper()).post(() -> {
                    button.setEnabled(true);
                    if(b) {
                        printMsg("** Secured store setup success **");
                        Timber.d("Setting up of secured store is success");
                    } else {
                        printException(exp);
                        Timber.d(exp, "Exception in setting up of secured store");
                    }
                });
            });
        } else if(cipherService.isSuperPinSetupRequired()) {
            cipherService.initialize((success, exp) -> {
                new Handler(Looper.getMainLooper()).post(() -> {
                    button.setEnabled(true);
                    if(success) {
                        printMsg("** Super pin setup success **");
                        Timber.d("Setup of super pin is success");
                    } else {
                        printException(exp);
                        Timber.d(exp, "Exception in setting up super pin");
                    }
                });
            });
        } else if(cipherService.isSuperPinSetupDone()){
            button.setEnabled(true);
            getTOTP();
        } else {
            button.setEnabled(true);
        }
    }

    private void getTOTP() {
        cipherService.getSuperPin((superPin, exp) -> {
            if(exp != null) {
                Log.e(TAG, "Exception in getting the super pin", exp);
                printException(exp);
            } else {
                Log.d(TAG, "The super pin is ...: " + superPin.getSuperPin());
                new Handler(Looper.getMainLooper()).post(() -> {
                    Toast.makeText(getApplicationContext(), "Super pin is: " + superPin.getSuperPin() + " and valid for " + superPin.getValidTill() + "ms", Toast.LENGTH_LONG).show();
                });
            }
        });
    }


    private void printException(final Throwable throwable) {
        final StringBuilder msg = new StringBuilder(throwable.getMessage());
        if(throwable instanceof ApolloException) {
            msg.append("; code: ").append(((ApolloException) throwable).getCode());
            msg.append("; traceId: ").append(((ApolloException) throwable).getTraceId());
            Timber.d("Trace id is: " + ((ApolloException) throwable).getTraceId());
        }
        new Handler(Looper.getMainLooper()).post(() -> {
            superPinExp.setText(msg.toString());
        });
    }


    private void printMsg(final String msg) {
        superPinExp.setText(msg);
    }
}