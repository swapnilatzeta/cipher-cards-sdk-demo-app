package in.zeta.apollo.cardsciphertest;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.JsonObject;

import org.jetbrains.annotations.Nullable;

import in.zeta.apollo.cards.CardsService;

public class CardDetailsActivity extends AppCompatActivity {
    private EditText enteredToken;
    private EditText enteredCardID;
    private Button setStaticPin;
    private Button registerSdk;
    private Button logout;
    private Button initSdk;
    private Button getCardView;
    private TextView initSdkStatus;
    private EditText etAch;
    private Button getSuperPin;

    private Button setStaticPinApi;
    private Button getCardPanDataApi;
    private Button getCardSensitiveDataApi;

    private TextView resultText;
    private FrameLayout templateLayout;
    private CardsService.GetSuperPinListener getSuperPinListener = (superPin, exp1) -> {
        if (superPin != null) {
            resultText.setText(superPin.getSuperPin() + " with validity of " + superPin.getValidTill() + "ms");
            Toast.makeText(this, superPin.getSuperPin(), Toast.LENGTH_LONG).show();
        } else {
            resultText.setText(exp1.getMessage());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_details);

        setupView();
    }

    private void setupView() {

        enteredToken = findViewById(R.id.etToken);
        setStaticPin = findViewById(R.id.btAuthToken);
        registerSdk = findViewById(R.id.btRegister);
        enteredCardID = findViewById(R.id.etCardID);
        getCardView = findViewById(R.id.btGetCardView);
        resultText = findViewById(R.id.tvOutput);
        templateLayout = findViewById(R.id.rootView);
        logout = findViewById(R.id.btLogout);
        initSdk = findViewById(R.id.btInitSdk);
        initSdkStatus = findViewById(R.id.tvSdkStatus);
        etAch = findViewById(R.id.etACH);
        getSuperPin = findViewById(R.id.btGetSensitiveInfo);
        setStaticPinApi = findViewById(R.id.btSetStaticPinAPI);
        getCardPanDataApi = findViewById(R.id.btGetCardPan);
        getCardSensitiveDataApi = findViewById(R.id.btCardSenseData);

        logout.setOnClickListener(view -> {
            MyApplication.getCardsService().logout();
            Toast.makeText(getApplicationContext(), "Logout success", Toast.LENGTH_SHORT).show();
        });

        registerSdk.setOnClickListener(view -> {

            if (enteredToken.getText().toString().trim().length() == 0) {
                new Handler(Looper.getMainLooper()).post(() -> {
                    Toast.makeText(getApplicationContext(), "Please enter tenantAuthToken", Toast.LENGTH_SHORT).show();
                });
                return;
            }
            if (etAch.getText().toString().trim().length() == 0) {
                Toast.makeText(this, "Please enter a valid ACH",
                        Toast.LENGTH_LONG).show();
                return;
            }
            resultText.setText("Registering SDK");
            MyApplication.getCardsService().authenticateSDK(enteredToken.getText().toString().trim(),
                    etAch.getText().toString().trim());
            resultText.setText("Registered SDK");
            String cardId = enteredCardID.getText().toString().trim();
            CardsService.SdkInitListener sdkInitListener = (success, exp) -> {
                initializeHandler(success, exp, cardId);
            };
            MyApplication.getCardsService().setupSDK(this, sdkInitListener, null);
        });

        getSuperPin.setOnClickListener(view -> {
            resultText.setText("Triggered your request ........");
            if (enteredCardID.getText().toString().trim().length() == 0) {
                new Handler(Looper.getMainLooper()).post(() -> {
                    Toast.makeText(getApplicationContext(), "Please enter a valid card id and tenant token", Toast.LENGTH_SHORT).show();
                });
                return;
            }
            MyApplication.getCardsService().getSuperPin(this, getSuperPinListener);
        });


        setStaticPin.setOnClickListener(view -> {
            resultText.setText("Triggered your request ........");
            if (enteredCardID.getText().toString().trim().length() == 0) {
                new Handler(Looper.getMainLooper()).post(() -> {
                    Toast.makeText(getApplicationContext(), "Please enter valid ifi, card id and tenant token", Toast.LENGTH_SHORT).show();
                });
                return;
            }
            MyApplication.getCardsService().setStaticPinWithUI(this, enteredCardID.getText().toString().trim(), new CardsService.SetPinListener() {
                @Override
                public void done(@Nullable JsonObject response, @Nullable Throwable exp) {
                    if (exp != null) {
                        resultText.setText(exp.getMessage());
                    } else {
                        resultText.setText("Pin Set Successful");
                    }
                }
            });

        });

        getCardView.setOnClickListener(v -> {

            MyApplication.getCardsService().getCardView(this, enteredCardID.getText().toString().trim(), new CardsService.GetViewListener() {
                @Override
                public void done(@Nullable View view, @Nullable Throwable exp) {
                    if(view != null) {
                        View rootView = view;
                        templateLayout.addView(rootView, 0);
                        templateLayout.setVisibility(View.VISIBLE);
                        logout.setVisibility(View.GONE);
                        registerSdk.setVisibility(View.GONE);
                        setStaticPin.setVisibility(View.GONE);
                        getSuperPin.setVisibility(View.GONE);
                        getCardView.setVisibility(View.GONE);
                    } else if (exp != null) {
                        resultText.setText(exp.getMessage());
                        Toast.makeText(CardDetailsActivity.this, exp.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        });


        setStaticPinApi.setOnClickListener(view -> {
            resultText.setText("Set Pin by API triggered");
            MyApplication.getCardsService().setStaticPin("1234", enteredCardID.getText().toString().trim(), (response, exp) -> {
                new Handler(Looper.getMainLooper()).post(() -> {
                    if(exp == null) {
                        resultText.setText("Set Pin by API success");
                    } else {
                        resultText.setText("Set Pin by API exp: " + exp.getMessage());
                    }
                });
            });
        });

        getCardPanDataApi.setOnClickListener(view -> {
            resultText.setText("Get card by API triggered");
            MyApplication.getCardsService().getCardData(enteredCardID.getText().toString().trim(), this, (cardsPan, exp) -> {
                new Handler(Looper.getMainLooper()).post(() -> {
                    if(exp == null) {
                        resultText.setText(cardsPan.toString());
                    } else {
                        resultText.setText("Get card data by API exp: " + exp.getMessage());
                    }
                });
            });
        });

        getCardSensitiveDataApi.setOnClickListener(view -> {
            resultText.setText("Get card sensitive data by API triggered");
            MyApplication.getCardsService().getCardSensitiveData(enteredCardID.getText().toString().trim(), this, (cardsPan, exp) -> {
                new Handler(Looper.getMainLooper()).post(() -> {
                    if(exp == null) {
                        resultText.setText(cardsPan.toString());
                    } else {
                        resultText.setText("Get card sensitive by API exp: " + exp.getMessage());
                    }
                });
            });
        });
    }


    private void initializeHandler(boolean success, Throwable exp, String cardId) {
        new Handler(Looper.getMainLooper()).post(() -> {
            initSdk.setEnabled(true);
            if (success) {
                resultText.setText("Initialized SDK");
            } else {
                resultText.setText(exp.getLocalizedMessage());
            }
        });
    }
}