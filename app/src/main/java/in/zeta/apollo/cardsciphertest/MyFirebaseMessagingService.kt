package `in`.zeta.apollo.cardsciphertest

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService: FirebaseMessagingService() {

    companion object {
        private const val TAG = "AppFirebaseMsg"
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.d(TAG, "Received a new push notification ...")
        // Check if the message contains a data payload ...
        if(!remoteMessage.data.isEmpty()) {
            Log.d(TAG, "The remote message is:" +  remoteMessage.data)

            //MyApplication.getCipherService().handleNotification(remoteMessage)

        } else {
            Log.d(TAG, "The notification payload is empty")
        }
    }

    override fun onNewToken(token: String) {

        // Invaliding the push token state in the local store ...
        //MyApplication.getCipherService().onPushTokenUpdated()
    }

}
